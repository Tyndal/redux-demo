import { connect } from "react-redux";
import { addComment } from "./actions";
import { CommentInit } from "./CommentInit";

export const mapDispatchToProps = dispatch => ({
  addComment: id => dispatch(addComment(prompt("Enter new comment")))
});

export default connect(
  null,
  mapDispatchToProps
)(CommentInit);
