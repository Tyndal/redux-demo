import { connect } from "react-redux";
import { Comment } from "./Comment";
import {
  thumbUpComment,
  thumbDownComment,
  removeComment,
  addComment
} from "./actions";

export const mapDispatchToProps = dispatch => ({
  thumbUpComment: id => dispatch(thumbUpComment(id)),
  thumbDownComment: id => dispatch(thumbDownComment(id)),
  removeComment: id => dispatch(removeComment(id)),
  addComment: () => dispatch(addComment(prompt("Enter new comment")))
});

export default connect(
  null,
  mapDispatchToProps
)(Comment);
