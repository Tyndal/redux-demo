import React from "react";

export const CommentInit = ({ text, addComment }) => (
  <li>
    <button onClick={() => addComment(text)}>Add comment</button>
  </li>
);

export default CommentInit;
