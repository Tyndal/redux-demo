import uuid from "uuid";

export const ADD_COMMENT = "ADD_COMMENT";
export const EDIT_COMMENT = "EDIT_COMMENT";
export const REMOVE_COMMENT = "REMOVE_COMMENT";
export const THUMB_UP_COMMENT = "THUMB_UP_COMMENT";
export const THUMB_DOWN_COMMENT = "THUMB_DOWN_COMMENT";

//=====================================================
//Regular functions plus creator syntax

export const addComment = function addComment(text) {
  return {
    type: ADD_COMMENT,
    text: text,
    id: uuid.v4()
  };
};

// const boundAddComment = text => dispatch(addComment(text));

// boundAddComment("Any new comment");

//=====================================================
export const editComment = function editComment(text, id) {
  return {
    type: EDIT_COMMENT,
    text: text,
    id: id
  };
};

// const boundEditComment = (text, id) => dispatch(editComment(text, id));

// boundEditComment("Any newly editet comment", id);

//=====================================================
export const removeComment = function removeComment(id) {
  return {
    type: REMOVE_COMMENT,
    id: id
  };
};

// const boundRemoveComment = id => dispatch(removeComment(id));

// boundRemoveComment(id);

//=====================================================
export const thumbUpComment = function thumbUpComment(id) {
  return {
    type: THUMB_UP_COMMENT,
    id: id
  };
};

// const boundThumbUpComment = id => dispatch(thumbUpComment(id));
// boundThumbUpComment(id);

//=====================================================
export const thumbDownComment = function thumbDownComment(id) {
  return {
    type: THUMB_DOWN_COMMENT,
    id: id
  };
};

// const boundThumbDownComment = id => dispatch(thumbDownComment(id));
// boundThumbDownComment(id);
