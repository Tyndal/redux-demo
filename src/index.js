import { Provider } from "react-redux";
import { createStore } from "redux";
import DevTools from "./DevTools";
import React from "react";
import ReactDOM from "react-dom";
import { addComment } from "./actions";
import { reducer } from "./reducer";
import App from "./App";

const store = createStore(reducer, DevTools.instrument());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

store.dispatch(addComment("First comment"));
store.dispatch(addComment("Second comment"));
