import React from "react";

export const Comment = ({
  text,
  votes,
  id,
  thumbUpComment,
  thumbDownComment,
  removeComment
}) => (
  <li>
    {text} <span>votes: {votes}</span>{" "}
    <button onClick={() => thumbUpComment(id)}>Like it!</button>
    <button onClick={() => thumbDownComment(id)}>Don't like it!</button>
    <button onClick={() => removeComment(id)}>Remove comment</button>
  </li>
);

export default Comment;
