import React from "react";
import CommentsListContainer from "./CommentsListContainer";
import "./App.css";
import DevTools from "./DevTools";
import CommentInitContainer from "./CommentInitContainer";

const App = () => {
  return (
    <div className="App">
      <CommentsListContainer />
      <CommentInitContainer />
      <DevTools />
    </div>
  );
};

export default App;
